package main;

import config.Config;
import config.HsqlDataSource;
import config.PostgresDataSource;
import model.Person;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import service.EmailService;
import service.TransferService;

public class Main {

    public static void main(String[] args) {

        var ctx = new AnnotationConfigApplicationContext(Config.class, PostgresDataSource.class, HsqlDataSource.class);

        try (ctx) {
            TransferService ts = ctx.getBean(TransferService.class);

            ts.transfer(1, "A", "B");

            ctx.getBean(EmailService.class).send("Hello");
//
//            System.out.println(ts);
//            System.out.println(ctx.getBean(TransferService.class));
//            System.out.println(System.identityHashCode(ts));
//            System.out.println(ts.getBankService());

//            PersonDao personDao = ctx.getBean(PersonDao.class);
//
//            Person john = new Person("John");
//
////            personDao.insertPerson(john);
//
//            System.out.println(personDao.getAllPersons());



        }

    }
}