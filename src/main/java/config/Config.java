package config;

import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import service.TransferService;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = {"service", "main", "aop"})
@PropertySource("classpath:/application.properties")
@EnableAspectJAutoProxy
public class Config {

//    @Bean
//    public TransferService transferService() {
//        return new TransferService();
//    }

    @Bean
    public JdbcTemplate getTemplate(DataSource ds) {
        return new JdbcTemplate(ds);
    }


//    @Bean
//    public DataSource dataSource(Environment env) {
//        DriverManagerDataSource ds = new DriverManagerDataSource();
//        ds.setDriverClassName("org.postgresql.Driver");
//        ds.setUsername(env.getProperty("postgres.user"));
//        ds.setPassword(env.getProperty("postgres.pass"));
//        ds.setUrl(env.getProperty("postgres.url"));
//
//        var populator = new ResourceDatabasePopulator(
//                new ClassPathResource("schema.sql"),
//                new ClassPathResource("data.sql"));
//
//        DatabasePopulatorUtils.execute(populator, ds);
//
//
//        return ds;
//    }


}